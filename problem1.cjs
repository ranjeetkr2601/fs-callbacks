const fs = require('fs');
const path = require('path');

function createDirectory(folderPath, callBackFunc) {

    fs.access(folderPath, (error) => {
        if (error) {
            fs.mkdir(path.join(__dirname, folderPath), (error) => {
                if (error) {
                    callBackFunc(error);
                } else {
                    callBackFunc(null);
                }
            });
        } else {
            console.log(`Directory "${folderPath}" already exists.`);
            callBackFunc(null);
        }
    });
    return folderPath;
}

function createAndDeleteFiles(folderPath, num = 10) {

    let fileNames = Array(num).fill(0).map((currentFile) => {
        let randomNum = Math.random().toString();
        let randomName = randomNum.substring(2, 10);
        currentFile = randomName + ".json";

        fs.open(path.join(folderPath, currentFile), 'w', (error) => {
            if (error) {
                console.error(error);
            }
            else {
                console.log(`${currentFile} Created`);
                fs.unlink(path.join(folderPath, currentFile), (error) => {
                    if (error) {
                        console.error(error);
                    }
                    else {
                        console.log(`${currentFile} Deleted`);
                    }
                });
            }
        });
        return currentFile;
    });
    return fileNames;
}

module.exports.createDirectory = createDirectory;
module.exports.createAndDeleteFiles = createAndDeleteFiles;