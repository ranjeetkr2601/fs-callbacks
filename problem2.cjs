const fs = require('fs');

//1. Read from a file.
function readFileFunc(pathToFile, callBackFunc){
    if(pathToFile === 'undefined'){
        pathToFile = 'lipsum.txt';
    }
    fs.readFile(pathToFile, 'utf8', (error, data) => {
        if(error){
            callback(error);
        }
        else{
            callBackFunc(null, data);
        }
    });
}

// Append new filenames to filenames.txt
function appendToFilenames(pathToFile){
    fs.appendFile('filenames.txt', "\n" + pathToFile , (error) => {
        if(error){
            console.error(error);
        } else {
            console.log(`${pathToFile} appended to filenames.txt`);
        }
    });
}

//2. convert data from file to uppercase and adds data to a new file.
function convertToUpperCase(callback){
    readFileFunc('lipsum.txt', (error, data) => {
        if(error){
            callback(error);
        } else {
            let pathToNewFile = 'lipsumUpperCase.txt';
            data = data.toUpperCase();
            fs.writeFile(pathToNewFile, data, (error) => {
                if(error){
                    callback(error);
                } else {
                    fs.writeFile('filenames.txt', pathToNewFile, (error) => {
                        if(error){
                            callback(error);
                        }
                        else {
                            console.log(`${pathToNewFile} written to filenames.txt`);
                            callback(null);
                        }
                    });
                }
            });
        }
    });
}

//3. convert data from file to lowercase and adds data to a new file.
function convertToLowerCase (callback){
    readFileFunc('lipsumUpperCase.txt', (error, data) => {
        if(error){
            callback(error);
        } else {
            data = data.toLowerCase()
                .split('. ')
                .join('.\n');
            fs.writeFile('sentenceSplit.txt', data, (error) => {
                if(error){
                    callback(error);
                } else {
                    appendToFilenames('sentenceSplit.txt');
                    callback(null);
                }
            });
        }
    });
}

//4. convert data from all new files and adds it to new a new file
function sortDataOfNewFiles(callback){
    readFileFunc('lipsumUpperCase.txt', (error, data1) => {
        if(error){
            callback(error);
        }
        else{
            readFileFunc('sentenceSplit.txt', (error, data2) => {
                if(error){
                    callback(error);
                } else {
                    let allData = data1 + data2;
                    allData = allData.split(" ");
                    allData.sort();
                    fs.writeFile('sortedData.txt', allData.toString(), (error) => {
                        if(error){
                            callback(error);
                        } else {
                            appendToFilenames('sortedData.txt');
                            callback(null);
                        }
                    });
                }    
            });
        }
    });
}

//5. delete those files whose names are present in filenames.txt.
function deleteFilesInFilenames(callback){
    readFileFunc('filenames.txt', (error, data) => {
        if(error){
            callback(error);
        } else {
            data.split('\n').forEach((currentFile) => {
                fs.unlink(currentFile, (error) => {
                    if(error){
                        callback(error);
                    }
                    else {
                        console.log(`${currentFile} deleted`);
                        callback(null);
                    }
                });
            });
        }
    });  
}

module.exports.readFileFunc = readFileFunc;
module.exports.convertToUpperCase = convertToUpperCase;
module.exports.convertToLowerCase = convertToLowerCase;
module.exports.sortDataOfNewFiles = sortDataOfNewFiles;
module.exports.deleteFilesInFilenames = deleteFilesInFilenames;