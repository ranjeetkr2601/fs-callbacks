const problem2 = require('../problem2.cjs');
const path = require('path');

problem2.convertToUpperCase((err) => {
    if(err){
        console.error(err);
    }
    else{
        problem2.convertToLowerCase((err)=>{
            if(err){
                console.error(err);
            } else {
                problem2.sortDataOfNewFiles((err) => {
                    if(err){
                        console.error(err);
                    }
                    else {
                        problem2.deleteFilesInFilenames((err) => {
                            if(err){
                                console.error(err);
                            }
                        });
                    }
                });
            } 
        });
    }  
});