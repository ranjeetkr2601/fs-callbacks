let problem1 = require('../problem1.cjs');

let folderPath = '/home/files';
problem1.createDirectory(folderPath, (err) => {
    if(err){
        console.error(err);
    }
    else {
        problem1.createAndDeleteFiles(folderPath, 5);
    }   
});